const submitButton = document.querySelector(".btn")
const password = document.querySelector(".password")
const passwordConfirm = document.querySelector(".confirm-password")
const eye = document.querySelector("#eye-1")
const eyeConfirm = document.querySelector("#eye-2")
const eyeSlash = document.querySelector(".fa-eye-slash")
const eyeSlashConfirm = document.querySelector("#eye-slash-2")
const errorPassword = document.querySelector(".uncorrect-password")

submitButton.addEventListener("click", (event) => {
    event.preventDefault()
    if (password.value === passwordConfirm.value) {
        alert("You are welcome")
        errorPassword.style.display = "none"
    } else {
        errorPassword.style.display = "block"
    }
})


eye.addEventListener("click", () => {
    if (password.type === "password") {
        password.type = "text"
        eye.style.display = "none"
        eyeSlash.style.display = "block"
    }
    eyeSlash.addEventListener("click", () => {
        eye.style.display = "block"
        eyeSlash.style.display = "none"
        password.type = "password"
    })
})

eyeConfirm.addEventListener("click", () => {
    if (passwordConfirm.type === "password") {
        passwordConfirm.type = "text"
        eyeConfirm.style.display = "none"
        eyeSlashConfirm.style.display = "block"
    }
    eyeSlashConfirm.addEventListener("click", () => {
        eyeConfirm.style.display = "block"
        eyeSlashConfirm.style.display = "none"
        passwordConfirm.type = "password"
    })
})
